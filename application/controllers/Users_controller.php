<?php 
   class Users_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      } 
      public function index()
      {
         $this->load->model('Users_Model');
           
           $data = array( 
               'username' => $this->input->post('username'), 
               'pwd' => $this->input->post('pwd') 
               ); 
               
           $resultat=$this->Users_Model-> login($data['username'],$data['pwd']); 
           if(isset($resultat['errorLogin'])){
               $data['error'] = " Username erroné ";
               $this->load->view('login',$data); 
           }
           if(isset($resultat['errorPassword'])){
               $data['error'] = " Mot de passe erroné ";
               // mbola tsy mety
               $this->load->view('login',$data); 
           }
   
           if(isset($resultat['user'])){
              redirect(site_url(array("accueil_controller","index")),"location");
           }
   
       }
      
   } 
?>
