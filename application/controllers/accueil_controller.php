<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class accueil_controller extends CI_Controller {

	private $categorie_affichee;
	private $row_num;
	private $page_num;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["menu"] = $this->load->view("menu_back");
        $this->load->view('index_back',$data);
	}
	function getParams(){
		$this->categorie_affichee=1;
		$this->row_num=0;
		$this->page_num=1;
		if(isset($_GET['row_num'])){
			if(!is_null($_GET['row_num'])){
				$this->row_num=$_GET['row_num'];
			}
		}
		if(isset($_GET['page_num'])){
			if(!is_null($_GET['page_num'])){
				$this->page_num=$_GET['page_num'];
			}
		}
		if(isset($_GET['categorie_affichee'])){
			if(!is_null($_GET['categorie_affichee'])){
				$this->categorie_affichee=$_GET['categorie_affichee'];
			}
		}
	}
	function createPagination(){
		$this->db->where('idcategorieplat', $this->categorie_affichee);
		$this->db->from('plat_details');
		$row_count=$this->db->count_all_results();
		$nbPage=((int)$row_count/3);
		$nbPage=$row_count%3!=0?$nbPage+1:$nbPage;
		
		$pages=array();
		array_push($pages,"<li><a href=\"#\" onclick=\"voir_ajax(1,".$this->categorie_affichee.",1)\" ><<</a></li>");
		if($this->page_num>1){array_push($pages,"<li><a href=\"#\" onclick=\"voir_ajax(".($this->page_num-1).",".$this->categorie_affichee.",".($this->row_num-3).")\"> Previous </a></li>");}
		
		for($i=$this->page_num;$i<$nbPage;$i++){
			$css="";
			if($i==0){$css="class=\"active\"";}
			array_push($pages,"<li ".$css."><a href=\"#\" onclick=\"voir_ajax(".($i).",".$this->categorie_affichee.",".($i*3-3).")\" >".$i."</a></li>");
		}
		if($this->page_num<$nbPage){array_push($pages,"<li><a href=\"#\" onclick=\"voir_ajax(".($this->page_num+1).",".$this->categorie_affichee.",".($this->row_num+3)."\" >Next</a></li>");}
		array_push($pages,"<li><a href=\"#\" onclick=\"voir_ajax(".$nbPage.",".$this->categorie_affichee.",".($row_count-($row_count%3)).")\" >>></a></li>");
		return $pages;
	}
}
